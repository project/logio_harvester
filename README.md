LogIO Harvester
===============

Log watchdog messages to [LogIO](http://logio.org);

Usage
-----

1. Install the module.
2. Go to admin/config/development/logging
3. Define your logio server settings
4. Connect to your logio web interface and watch logs appear!

Note
----

This module does __not__ include the LogIO server or client. It is strickly a "harvester" with which you can send log data to an existing server setup.